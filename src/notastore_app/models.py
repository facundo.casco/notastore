from django.db import models
from django.utils import timezone


class Store(models.Model):
    name = models.CharField(max_length=64)
    details = models.TextField(blank=True)
    logo = models.ImageField(upload_to='store/logos', blank=True, null=True)
    catalog = models.FileField(upload_to='store/files', blank=True, null=True)

    class Meta:
        ordering = ('name', )

class StoreProduct(models.Model):
    name = models.CharField(max_length=64)
    store = models.ForeignKey(Store)
    position = models.SmallPositiveInteger(default=0)
    rate = models.SmallPositiveInteger(default=4)
    created_at = models.DateTimeField(default=timezone.now)
