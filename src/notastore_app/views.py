from django.views.generic import ListView

from notastore_app.models import Store, StoreProduct


class StoresList(ListView):
    model = Store
    queryset = Store.objects.all()
    template_name = "stores_list.html"


class StoreHomeView(ListView):
    model = Store
    queryset = Store.objects.all()
    template_name = 'store_home.html'

    def get_top_products(self):
        store = Store.objects.get(pk=self.kwargs["pk"])
        by_date = {"by_date": store.products.order_by("created_at")[:12]}
        by_rate = {"by_rate": store.products.order_by("rate")[:12]}
        return by_date | by_rate

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        return {
            **ctx, 
            "store": Store.objects.get(pk=self.kwargs['pk']),
            "products": StoreProduct.objects.order_by("name").filter(store = self.kwargs['pk']),
            "best_rate": max(x.rate for x in Store.objects.get(pk=self.kwargs["pk"]).products.all()),
        }
